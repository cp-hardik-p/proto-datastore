package com.example.protodatastore

import androidx.datastore.core.CorruptionException
import androidx.datastore.core.Serializer
import com.google.protobuf.InvalidProtocolBufferException
import java.io.InputStream
import java.io.OutputStream


object EmployeePreferencesSerializer : Serializer<EmployeePreference> {
    override val defaultValue: EmployeePreference = EmployeePreference.getDefaultInstance()

    @Suppress("BlockingMethodInNonBlockingContext")
    override suspend fun readFrom(input: InputStream): EmployeePreference {
        try {
            return EmployeePreference.parseFrom(input)
        } catch (exception: InvalidProtocolBufferException) {
            throw CorruptionException("Cannot read proto.", exception)
        }
    }

    @Suppress("BlockingMethodInNonBlockingContext")
    override suspend fun writeTo(t: EmployeePreference, output: OutputStream) = t.writeTo(output)
}