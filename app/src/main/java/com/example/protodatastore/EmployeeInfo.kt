package com.example.protodatastore

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.dataStore
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.Flow

private const val DATA_STORE_FILE_NAME = "emp_prefs.pb"

class EmployeeInfo(private val context: Context) {


    private val Context.employeeProtoDataStore: DataStore<EmployeePreference> by dataStore(
        fileName = DATA_STORE_FILE_NAME,
        serializer = EmployeePreferencesSerializer
    )

    //Writing to a proto data store
    suspend fun saveEmployeeInfo(eName: String, eDesignation: String) {
        context.employeeProtoDataStore.updateData { employeeData ->
            employeeData.toBuilder()
                .setEmpName(eName)
                .setEmpDesignation(eDesignation)
                .build()

        }
    }

    //Reading employee object from a proto data store
    val employeeInfo: Flow<EmployeePreference> = context.employeeProtoDataStore.data
        .map {
            it
        }

    //Reading employee object property empName from a proto data store
    val empName: Flow<String> = context.employeeProtoDataStore.data
        .map {
            it.empName
        }

    //Reading employee object property empDesignation from a proto data store
    val empDesignation: Flow<String> = context.employeeProtoDataStore.data
        .map {
            it.empDesignation
        }
}

