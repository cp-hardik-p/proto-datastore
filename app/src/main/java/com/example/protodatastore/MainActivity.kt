package com.example.protodatastore

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import com.example.protodatastore.ui.theme.ProtoDataStoreTheme
import com.example.protodatastore.ui.theme.ThemeColor
import kotlinx.coroutines.launch

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ProtoDataStoreTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                   EmployeeData()
                }
            }
        }
    }
}

@Composable
fun EmployeeData(){
    Column(
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier.fillMaxWidth()
    ) {
        val context = LocalContext.current
        val scope = rememberCoroutineScope()
        //val employeeInfo = EmployeeInfo(context)
        val employeeInfo = remember(context){EmployeeInfo(context)}

        var ename by rememberSaveable{ mutableStateOf("") }
        var edesignation by rememberSaveable { mutableStateOf("") }

        Text(
            text = "Hello, ${employeeInfo.empName.collectAsState("").value}!",
            modifier = Modifier.padding(10.dp)
        )
        Text(
            text = "Your Designation is, ${employeeInfo.empDesignation.collectAsState(0).value}!",
            modifier = Modifier.padding(10.dp)
        )

        OutlinedTextField(value = ename,
            onValueChange = { ename = it },
            label = { Text("EName") },
            colors = TextFieldDefaults.textFieldColors(
                backgroundColor = Color.Transparent,
                focusedIndicatorColor = ThemeColor,
                focusedLabelColor= ThemeColor
            )
        )

        OutlinedTextField(value = edesignation,
            onValueChange = { edesignation = it },
            label = { Text("Designation") },
            colors = TextFieldDefaults.textFieldColors(
                backgroundColor = Color.Transparent,
                focusedIndicatorColor = ThemeColor,
                focusedLabelColor= ThemeColor
            )
        )


        Button(onClick = {
            scope.launch {
                employeeInfo.saveEmployeeInfo(ename, edesignation)
            }
        },
            colors = ButtonDefaults.buttonColors(
                backgroundColor = ThemeColor,
                contentColor = Color.White
            )
        ) {
            Text(text = "Submit data")
        }
    }
}

